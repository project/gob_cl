import gulp from 'gulp';
import gulpClean from 'gulp-clean';
import gulpSass from 'gulp-sass';
import cleanCSS from 'gulp-clean-css';
import autoprefixer from 'gulp-autoprefixer';
import rename from 'gulp-rename';
import sourcemaps from 'gulp-sourcemaps';
import wrap from 'gulp-wrap';

import { rollup } from 'rollup';
import rollupBabel from 'rollup-plugin-babel';

import browserSync from 'browser-sync';

const server = browserSync.create();

const copyFonts = () => (
  gulp.src('./node_modules/@gobdigital-cl/gob.cl/src/fonts/*')
    .pipe(gulp.dest('./fonts'))
);

const copyImages = () => (
  gulp.src('./node_modules/@gobdigital-cl/gob.cl/src/img/**/*')
    .pipe(gulp.dest('./img'))
);

const copySrcJs = () => (
  gulp.src('./node_modules/@gobdigital-cl/gob.cl/src/js/**/*')
    .pipe(gulp.dest('./js-src'))
);

const buildCss = () => (
  gulp.src('./sass/styles.scss')
    .pipe(sourcemaps.init())
    .pipe(gulpSass({
      outputStyle: 'nested',
      precision: 10
    }).on('error', gulpSass.logError))
    .pipe(autoprefixer())
    .pipe(cleanCSS())
    .pipe(rename('styles.css'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./css'))
    .pipe(server.stream())
);

const buildJs = () => (
  rollup({
    input: './js-src/app.js',
    plugins: [
      rollupBabel({
        presets: [['@babel/env', { modules: 'false' }]],
        exclude: 'node_modules/**'
      })
    ]
  })
    .then(bundle => (
      bundle.write({
        file: './js/includes/gob.cl.js',
        format: 'iife'
      })
    ))

);

const wrapJs = () => (
  gulp.src('./js/includes/gob.cl.js')
    .pipe(wrap('(function ($) {<%= contents %>})(jQuery);', {}, { parse: false }))
    .pipe(gulp.dest("./js/includes"))
);

const watchScss = () => gulp.watch('./sass/**/*.scss', gulp.series(buildCss));

const deleteSrcJs = () => (
  gulp.src([
    './js-src'
  ], { allowEmpty: true })
    .pipe(gulpClean())
);

gulp.task('copy', gulp.parallel(copyFonts, copyImages));

gulp.task('build:js', gulp.series(copySrcJs, buildJs, deleteSrcJs, wrapJs));

gulp.task('build:css', gulp.series(buildCss));

gulp.task('build', gulp.parallel('build:js', 'build:css', 'copy'));

gulp.task('watch', gulp.series('build:css', gulp.parallel(watchScss)));
